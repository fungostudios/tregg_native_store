﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Collections.Generic;
using Microsoft.Phone.Tasks;

namespace AppUtils
{
    public class StoreUtils
    {
        #region App store link

        public const string STORE_APP_ID = "STORE_APP_ID";
        public static void GoToAppStorePage(Dictionary<string, object> parameters)
        {
            if (!parameters.ContainsKey(STORE_APP_ID))
                return;

            MarketplaceDetailTask marketplaceDetailTask = new MarketplaceDetailTask();

            marketplaceDetailTask.ContentIdentifier = parameters[STORE_APP_ID].ToString();
            marketplaceDetailTask.ContentType = MarketplaceContentType.Applications;

            marketplaceDetailTask.Show();
        }

        #endregion
    }
}
